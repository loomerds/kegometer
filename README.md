# KEGOMETER: A Pomodoro for Learning

***KEGOMETER*** is a digital pomodoro for use by teachers and students to support focus mode/diffuse mode learning sessions. It was written in JavaScript using the React Framework. 

In addition to letting the user adjust the length of learning sessions (focus mode learning) and breaks (diffuse mode learning), it lets the user select which sounds will be played to announce the end of sessions and breaks. What could be more fun?

![The KEGOMETER Pomodoro Page](/src/images/KegometerPomodoroPage.png) ![The KEGOMETER Settings Page](/src/images/KegometerSettingsPage.png)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). 
See the CreateReactAppREADME for more details.

