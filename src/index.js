import React from 'react';
import 'normalize.css';
import './index.scss';
import './components/App.scss';
import App from './components/App';
import * as serviceWorker from './serviceWorker';
import {render} from "react-dom";

render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
