import React from 'react';
import { FaToggleOff, FaToggleOn, FaSyncAlt } from 'react-icons/fa';

class Controls extends React.Component {

    render() {
      return (
        <div className="timer-control">
          <div id="start_stop" onClick={this.props.handleTimerOffOn}>
            <FaToggleOff id="toggle-off" className={this.props.onSwitch} />
            <FaToggleOn id="toggle-on" className={this.props.offSwitch} />
          </div>
          <div id="reset" onClick={this.props.handleReset}>
            <FaSyncAlt id="reset-icon" className="reset-icon hover-icon" />
          </div>
        </div>
      )
    }
  }

  export default Controls;