import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing playBeep", ()=> {
    
    test("plays the default timer sound or a random sound if more than one sound has been selected", ()=> {
        
        window.HTMLMediaElement.prototype.load = () => { /* do nothing */ };
        window.HTMLMediaElement.prototype.play = () => { /* do nothing */ };
        window.HTMLMediaElement.prototype.pause = () => { /* do nothing */ };
        window.HTMLMediaElement.prototype.addTextTrack = () => { /* do nothing */ };
        
        let tree = create(<App />);
        let instance = tree.getInstance();
        if(instance.state.soundIndexes.length<2) {
            expect(instance.playBeep()).toBe("played the only time sound selected");
        } else {
            expect(instance.playBeep()).toBe("played a randomly selected timer sound from a list of two or more selected timersounds");
        }
    });
});