import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing handleBreakTimeUp", ()=> {
    test("increments breakLength by 1", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        expect(instance.handleBreakTimeUp()).toStrictEqual([instance.state.breakLength, instance.state.breakLength]);
    });
});