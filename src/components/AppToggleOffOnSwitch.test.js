import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing toggleOffOnSwitch", ()=> {
    test("toggles the state of the timer start/stop icons", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        if(instance.state.onSwitch === "toggle-visible hover-icon") {
            expect(instance.toggleOffOnSwitch()).toStrictEqual([true, true]);
        }
        else {
            expect(instance.toggleOffOnSwitch()).toStrictEqual(true, true);
        }
    });
});