import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing toggleSettingsPage", ()=> {
    test("toggles the visibility of the pomodoro page and the settings page", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        if(instance.state.settingsPage === "page-visible") {
            expect(instance.toggleSettingsPage()).toStrictEqual([true, true]);
        }
        else {
            expect(instance.toggleSettingsPage()).toStrictEqual([true, true]);
        }
    });
});