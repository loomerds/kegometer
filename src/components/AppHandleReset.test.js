import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing handleReset", ()=> {
    test("resets breakLength, sessionLength, timerIsRunning, timerIsSession, timerLabel, and seconds to their default values", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        expect(instance.handleReset()).toStrictEqual([5, 25, false, true, "Session", 1500]);
    });
});