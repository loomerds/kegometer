import React from 'react';
import { FaAngleDown, FaAngleUp } from 'react-icons/fa';

class Break extends React.Component {
  
    render() {
      return (
        <div className="length-control">
          <div id="break-label">Break Length</div>
          <div className="setting-row">
            <FaAngleDown id="break-decrement" className="hover-icon" onClick={this.props.handleBreakTimeDown}/>
            <div id="break-length">{this.props.breakLength}</div>
            <FaAngleUp id="break-increment" className="hover-icon" onClick={this.props.handleBreakTimeUp}/>
          </div>
        </div>
      )
    }
  }

  export default Break;