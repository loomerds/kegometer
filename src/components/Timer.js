import React from 'react';

class Timer extends React.Component {
  
    formatTime = ()=> {
      let localMinutes = Math.floor(this.props.seconds / 60);
      let localSeconds = this.props.seconds - (localMinutes * 60);
      if(localMinutes < 10) {
        localMinutes = "0" + localMinutes;
      } 
      if(localSeconds < 10) {
        localSeconds = "0" + localSeconds;
      }
      return (localMinutes + ":" + localSeconds);
    }
    
    render() {
      return (
        <div className="timer-box">
          <div id="timer-content">
            <div id="timer-label">
              {this.props.timerLabel}
            </div>
            <div id='time-left'>
              {this.formatTime()}
            </div>
          </div>
        </div>
      )
    }
  }

  export default Timer;