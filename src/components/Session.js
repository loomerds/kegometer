import React from 'react';
import { FaAngleDown, FaAngleUp } from 'react-icons/fa';

class Session extends React.Component {
  
    render() {
      return (
        <div className="length-control">
        <div id="session-label">Session Length</div>
          <div className="setting-row">
            <FaAngleDown id="session-decrement" className="hover-icon" onClick={this.props.handleSessionTimeDown}/>
            <div id="session-length">{this.props.sessionLength}</div>
            <FaAngleUp id="session-increment" className="hover-icon" onClick={this.props.handleSessionTimeUp}/>
           </div>
        </div>
      )
    }
  }

  export default Session;