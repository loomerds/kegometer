import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing handleTimerOffOn", ()=> {
    test("toggles the timer icon between on and off images", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        if(instance.state.timerIsRunning===true) {
            expect(instance.handleTimerOffOn()).toBe([false]);
        } else {
            expect(instance.handleTimerOffOn()).toBe(true);
        }
    });
});