import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing manageSessionBreak", ()=> {
    test("toggles state of timerIsSession", ()=> {

        window.HTMLMediaElement.prototype.load = () => { /* do nothing */ };
        window.HTMLMediaElement.prototype.play = () => { /* do nothing */ };
        window.HTMLMediaElement.prototype.pause = () => { /* do nothing */ };
        window.HTMLMediaElement.prototype.addTextTrack = () => { /* do nothing */ };

        let tree = create(<App />);
        let instance = tree.getInstance();
        if(instance.state.timerIsSession===true) {
            expect(instance.manageSessionBreak()).toBe(false);
        } else {
            expect(instance.manageSessionBreak()).toBe(true);
        }
    });
});