import React from 'react';
import Break from './Break';
import Session from './Session';
import Timer from './Timer';
import Controls from './Controls';
import Settings from './Settings';
import sounds from "./sounds.js";
import { MdMenu } from 'react-icons/md';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      beepSounds: sounds,
      beepSound: sounds[0],
      soundIndexes: [0],
      breakLength: 5,
      sessionLength: 25,
      timerIsRunning: false,
      timerIsSession: true,
      timerLabel: "Session",
      seconds: 1500,
      counter: null,
      onSwitch: "toggle-visible hover-icon",
      offSwitch: "toggle-hidden hover-icon",
      settingsPage: "page-hidden",
      pomodoroPage: "page-visible",
      interval: 1000
    }
  };
  
  inputsForm = React.createRef();  

  handleReset = ()=> {
    this.stopCounter();
    this.setState({
      breakLength: 5,
      sessionLength: 25,
      timerIsRunning: false,
      timerIsSession: true,
      timerLabel: "Session",
      seconds: 1500
    });
    return [this.state.breakLength,
            this.state.sessionLength,
            this.state.timerIsRunning,
            this.state.timerIsSession,
            this.state.timerLabel,
            this.state.seconds
           ]
  };

  handleBreakTimeDown = ()=> {
    let localBreakLength = this.state.breakLength;
    if(this.state.breakLength > 1 && this.state.timerIsRunning === false) {
      this.setState((prevState, props)=> {return {breakLength: prevState.breakLength - 1}});
      localBreakLength = localBreakLength - 1;
      if(this.state.timerIsSession === false) {
        this.resetSeconds(localBreakLength);
      }
    }
    return [localBreakLength, this.state.breakLength];
  };

  handleBreakTimeUp = ()=> {
    let localBreakLength = this.state.breakLength;
    if(this.state.breakLength < 60 && this.state.timerIsRunning === false) {
      this.setState((prevState, props)=> {return {breakLength: prevState.breakLength + 1}});
      localBreakLength = localBreakLength + 1;
      if(this.state.timerIsSession === false) {
        this.resetSeconds(localBreakLength);
      }
    }
    return [localBreakLength, this.state.breakLength];
  };
   
  handleSessionTimeDown = ()=> {
    let localSessionLength = this.state.sessionLength;
    if(this.state.sessionLength > 1 && this.state.timerIsRunning === false) {
      this.setState((prevState, props)=> {return {sessionLength: prevState.sessionLength - 1}});
      localSessionLength = localSessionLength - 1;
      if(this.state.timerIsSession === true) {
        this.resetSeconds(localSessionLength);
      }
    }
    return [localSessionLength, this.state.sessionLength];
  };
  
  handleSessionTimeUp = ()=> {
    let localSessionLength = this.state.sessionLength;
    if(this.state.sessionLength < 60 && this.state.timerIsRunning === false) { 
      this.setState((prevState, props)=> {return {sessionLength: prevState.sessionLength + 1}});
      localSessionLength = localSessionLength + 1;
      if(this.state.timerIsSession === true) {
        this.resetSeconds(localSessionLength);
      }
    }
    return [localSessionLength, this.state.sessionLength];
  };
   
  countDown = ()=> {
    let prevSeconds = this.state.seconds;
    this.setState((prevState,props)=> {return {seconds: prevState.seconds - 1}});
    if(this.state.seconds === -1) {
      this.manageSessionBreak();
    }
    return [prevSeconds, this.state.seconds];
  };
  
  toggleTimerLabel = ()=> {
    if(this.state.timerIsSession === true) {
      this.setState({timerLabel: "Session"});
    } else {
      this.setState({timerLabel: "Break"});
    }
    return this.state.timerLabel;
  };
  
  startCounter = ()=> {
    let counterValue = this.state.counter;
    let counter = setInterval(this.countDown, this.state.interval);
    this.setState({counter: counter});
    return [this.state.counter!==counterValue, this.state.interval];
  }
  
  stopCounter = ()=> {
    let counterValue = this.state.counter;
    clearInterval(this.state.counter);
    return this.state.counter===counterValue;
  };

  resetSeconds = (sessionLength)=> {
    let prevSeconds = this.state.seconds;
    this.setState({seconds: sessionLength * 60});
    return [prevSeconds, this.state.seconds];
  };
  
  manageSessionBreak = ()=> {
    this.state.timerIsSession===false ? this.resetSeconds(this.state.sessionLength) 
                                      : this.resetSeconds(this.state.breakLength);
    this.setState((prevState, props)=> {return {timerIsSession: !prevState.timerIsSession}});
    this.toggleTimerLabel();
    this.playBeep();
    return this.state.timerIsSession;
  };

  handleTimerOffOn = ()=> {
    this.state.timerIsRunning===false ? this.startCounter() : this.stopCounter();
    this.toggleOffOnSwitch();
    this.setState((prevState, props)=> {
      if(prevState.timerIsRunning === true) {
        return {timerIsRunning: false};
      } else {
        return {timerIsRunning: true};
      }
    });
    return this.state.timerIsRunning;
  };
  
  toggleOffOnSwitch = ()=> {
    if(this.state.onSwitch === "toggle-visible hover-icon") {
      this.setState({
        onSwitch: "toggle-hidden hover-icon",
        offSwitch: "toggle-visible hover-icon"
      });
      return [this.state.onSwitch==="toggle-hidden hover-icon", 
              this.state.offSwitch==="toggle-visible hover-icon"];
    } else {
      this.setState({
        onSwitch: "toggle-visible hover-icon",
        offSwitch: "toggle-hidden hover-icon"
      });
      return [this.state.onSwitch==="toggle-visible hover-icon", 
              this.state.offSwitch==="toggle-hidden hover-icon"];
    }
  };

  playBeep = ()=> {
    let newSoundFound = false;
    if(this.state.soundIndexes.length<2){
      this.state.beepSounds[this.state.soundIndexes[0]].play();
      return "played the only time sound selected"
    }
    while(newSoundFound===false) {
      let randomIndex = Math.floor(Math.random() * this.state.soundIndexes.length);
      if(this.state.beepSound===this.state.beepSounds[this.state.soundIndexes[randomIndex]]) {
        continue;
      } else {
        newSoundFound = true;
        this.state.beepSounds[this.state.soundIndexes[randomIndex]].play();
        this.setState({beepSound: this.state.beepSounds[this.state.soundIndexes[randomIndex]]});
        return "played a randomly selected timer sound from a list of two or more selected timersounds";
      }
    }
  };
  
  handleCheckBoxChange = (event)=> {
    let selectedSoundsArray = (()=> {
      let referenceToForm = this.inputsForm.current.refs.inputsForm;
      let tempArray = [];
      for(let r=0; r<referenceToForm.length; r++) {
        if(referenceToForm[r].checked)
        tempArray[tempArray.length] = referenceToForm[r]; 
      }
      return tempArray;
    })();
    let currentSoundsIndexes = [];
    for(let i = 0; i < selectedSoundsArray.length; i++) {
      currentSoundsIndexes[currentSoundsIndexes.length] = parseInt(selectedSoundsArray[i].value);
    }
    this.setState({soundIndexes: currentSoundsIndexes})
    if(currentSoundsIndexes.length===0) {
      this.inputsForm.current.refs.inputsForm[0].checked = "checked";
    }
  };

  toggleSettingsPage = ()=> {
    if(this.state.settingsPage === "page-visible") {
      this.setState({
        settingsPage: "page-hidden",
        pomodoroPage: "page-visible"
      })
      return [this.state.settingsPage==="page-hidden", 
              this.state.pomodoroPage==="page-visible"];
    } else {
      this.setState({
        settingsPage: "page-visible",
        pomodoroPage: "page-hidden"
      })
      return [this.state.settingsPage==="page-visible", 
              this.state.pomodoroPage==="page-hidden"]
    }
  };
  
  render() {
    return (
      <div id = "app-box-wrapper">
        <div id="app-box">
          <div id="app-label">
            <div id="spacer-left" className="spacer">
            <MdMenu id="settings-menu" className="hover-icon" onClick={this.toggleSettingsPage}/>
            </div>
            <div id="title-area">
              <h3>KEGOMETER</h3>
              <p className="subtitle">A Pomodoro for Learning</p>
            </div>
            <div id="spacer-right" className="spacer">
            </div>
          </div>
          <div id="pomodoro-box" className={this.state.pomodoroPage}>
            <div id="settings">
              <Break 
                breakLength={this.state.breakLength} 
                handleBreakTimeUp={this.handleBreakTimeUp} 
                handleBreakTimeDown ={this.handleBreakTimeDown} 
              />
              <Session 
                sessionLength={this.state.sessionLength} 
                handleSessionTimeUp={this.handleSessionTimeUp} 
                handleSessionTimeDown ={this.handleSessionTimeDown} 
              />
            </div>
            <Timer 
              seconds={this.state.seconds} 
              timerIsSession={this.state.timerIsSession} 
              timerLabel={this.state.timerLabel} 
              sessionLength={this.state.sessionLength}
            />
            <Controls 
              breakLength={this.state.breakLength} 
              sessionLength={this.state.sessionLength} 
              timeLeft={this.state.timeLeft} 
              handleReset={this.handleReset} 
              handleTimerOffOn={this.handleTimerOffOn} 
              timerCountDown={this.timerCountDown} 
              onSwitch={this.state.onSwitch} 
              offSwitch={this.state.offSwitch} 
            />
          </div>
          <div id="settings-box" className={this.state.settingsPage}>
            <Settings 
              ref={this.inputsForm}
              
              beepSounds={this.state.beepSounds}
              handleCheckBoxChange={this.handleCheckBoxChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
