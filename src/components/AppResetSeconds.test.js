import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing restSeconds", ()=> {
    test("resets the value of seconds to seconds * 60", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        expect(instance.resetSeconds(instance.state.sessionLength)).toStrictEqual([instance.state.seconds, instance.state.seconds]);
    });
});