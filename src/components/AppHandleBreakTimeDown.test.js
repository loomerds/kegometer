import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing handleBreakTimeDown", ()=> {
    test("decrements BreakLength by 1", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        expect(instance.handleBreakTimeDown()).toStrictEqual([instance.state.breakLength, instance.state.breakLength]);
    });
});