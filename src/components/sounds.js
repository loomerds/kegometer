import beepSound from "../apps/assets/audio/beepSound.mp3";
import oceanLiner from "../apps/assets/audio/oceanLiner.mp3";
import beethoven5th from "../apps/assets/audio/beethoven5th.mp3";
import carAlarm from "../apps/assets/audio/carAlarm.mp3";
import ambulence from "../apps/assets/audio/ambulence.mp3";
import redAlert from "../apps/assets/audio/redAlert.mp3";

const sounds = [
    new Audio(beepSound),
    new Audio(oceanLiner),
    new Audio(beethoven5th),
    new Audio(carAlarm),
    new Audio(ambulence),
    new Audio(redAlert)
];

export default sounds;