import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing countDown", ()=> {
    test("decrements the value of seconds by 1", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        expect(instance.countDown()).toStrictEqual([instance.state.seconds+1, instance.state.seconds]);
    });
});
