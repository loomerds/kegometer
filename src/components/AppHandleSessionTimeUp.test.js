import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing handleSessionTimeUp", ()=> {
    test("increments sessionLength by 1", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        expect(instance.handleSessionTimeUp()).toStrictEqual([instance.state.sessionLength, instance.state.sessionLength]);
    });
});