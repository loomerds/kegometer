import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing toggleTimerLabel", ()=> {
    test("toggles the value of timerLabel between 'Session' and 'Break'", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        if(instance.state.timerIsSession==="Session") {
            expect(instance.toggleTimerLabel()).toBe("Break");
        }
        else {
            expect(instance.toggleTimerLabel()).toBe("Session");
        }
    });
});