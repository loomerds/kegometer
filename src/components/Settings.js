import React from 'react';
import { FaPlayCircle } from 'react-icons/fa';

class Settings extends React.Component {
    render = ()=> (
        <div id="settings-container">
            <h5>Sound Settings</h5>
            <p>
                Click one or more boxes below to select which sound(s) you want to hear when each Session/Break ends.
            </p>
            <p>
                If you select a single sound, that sound will play when each Session/Break ends.
                If you select more than one sound, the sound that plays when a Session/Break ends will be 
                randomly chosen from the sounds you have selected.
            </p>
            <form ref="inputsForm" id="beep-selections">
                <div className="check-box-row">
                    <FaPlayCircle id="play-0" className="play-button hover-icon" 
                        onClick={()=> {this.props.beepSounds[0].play()}}/>
                    <input type="checkbox" id="checkbox-0" className="check-box-item" 
                        name="default" value="0" defaultChecked onChange={this.props.handleCheckBoxChange} />
                    <label htmlFor="checkbox-0" className="check-box-label" />
                    <label htmlFor="checkbox-0" className="label-text hover-icon">Default</label>
                </div>
                <div className="check-box-row">
                    <FaPlayCircle id="play-1" className="play-button hover-icon" 
                        onClick={()=> {this.props.beepSounds[1].play()}}/>
                    <input type="checkbox" id="checkbox-1" className="check-box-item"
                        name="oceanLiner" value="1" onChange={this.props.handleCheckBoxChange} />
                    <label htmlFor="checkbox-1" className="check-box-label" />
                    <label htmlFor="checkbox-1" className="label-text hover-icon">Ocean Liner</label>
                </div>
                <div className="check-box-row">
                    <FaPlayCircle id="play-2" className="play-button hover-icon" 
                        onClick={()=> {this.props.beepSounds[2].play()}}/>   
                    <input type="checkbox" id="checkbox-2" className="check-box-item" 
                        name="beethoven5th" value="2" onChange={this.props.handleCheckBoxChange}/>
                    <label htmlFor="checkbox-2" className="check-box-label" />
                    <label htmlFor="checkbox-2" className="label-text hover-icon">Beethoven's 5th</label>
                </div>
                <div className="check-box-row">
                    <FaPlayCircle id="play-3" className="play-button hover-icon" 
                        onClick={()=> {this.props.beepSounds[3].play()}}/>
                    <input type="checkbox" id="checkbox-3" className="check-box-item" 
                        name="carAlarm" value="3" onChange={this.props.handleCheckBoxChange}/>
                    <label htmlFor="checkbox-3" className="check-box-label" />
                    <label htmlFor="checkbox-3" className="label-text hover-icon">Car Alarm</label>
                </div>
                <div className="check-box-row">
                    <FaPlayCircle id="play-4" className="play-button hover-icon" 
                        onClick={()=> {this.props.beepSounds[4].play()}}/>
                    <input type="checkbox" id="checkbox-4" className="check-box-item" 
                        name="ambulence" value="4" onChange={this.props.handleCheckBoxChange}/>
                    <label htmlFor="checkbox-4" className="check-box-label" />
                    <label htmlFor="checkbox-4" className="label-text hover-icon">Ambulence</label>
                </div>
                <div className="check-box-row">
                    <FaPlayCircle id="play-5" className="play-button hover-icon" 
                        onClick={()=> {this.props.beepSounds[5].play()}}/>
                    <input type="checkbox" id="checkbox-5" className="check-box-item" 
                        name="redAlert" value="5" onChange={this.props.handleCheckBoxChange}/>
                    <label htmlFor="checkbox-5" className="check-box-label" />
                    <label htmlFor="checkbox-5" className="label-text hover-icon">Red Alert</label>
                </div>
            </form>
            <div className="bottom-bar"></div>
        </div>
    )
}

export default Settings;