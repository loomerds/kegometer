import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing stopCounter", ()=> {
    test("stops the counter from decrementing", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        expect(instance.stopCounter()).toBe(true);
    });
});
