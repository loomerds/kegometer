import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing startCounter", ()=> {
    test("starts the counter decrementing and updates its interval state", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        expect(instance.startCounter()).toStrictEqual([true, instance.state.interval]);
    });
});
