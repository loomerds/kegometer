import React from "react";
import App from "./App";
import { create } from 'react-test-renderer';

describe("Testing handleSessionTimeDown", ()=> {
    test("decrements sessionLength by 1", ()=> {
        let tree = create(<App />);
        let instance = tree.getInstance();
        expect(instance.handleSessionTimeDown()).toStrictEqual([instance.state.sessionLength, instance.state.sessionLength]);
    });
});